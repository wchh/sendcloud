<?php
/**
 * SendSms.php
 * User: wuchunhe
 * Date: 2021/5/27 2:54 下午
 */

//短信发送
namespace sendcloud;

use sendcloud\lib\SendCloudSMS;
use sendcloud\lib\util\SmsMsg;
use sendcloud\lib\util\VoiceMsg;
use sendcloud\lib\util\MsgType;

class SendSms
{
    protected $send_sms;
    protected $sms_msg;

    public function __construct()
    {
        $this->send_sms = new SendCloudSMS();
        $this->sms_msg = new SmsMsg();
    }

    /**
     * 短信
     * @param array $phone
     * @param array $vars
     * @param int $smsTemplateId
     * @return mixed|null
     * author: wuchunhe
     * Date: 2021/6/16 3:18 下午
     */
    function sendsms($phone = [], $vars = [], $smsTemplateId = 0)
    {
        $this->sms_msg->addPhoneList($phone);
        $this->sms_msg->addMapVars($vars);
        $this->sms_msg->setTemplateId($smsTemplateId);
        $this->sms_msg->setTimestamp(time());
        $resonse = $this->send_sms->send($this->sms_msg);
        return $resonse->body();

    }

    /**
     * 彩信
     * @param array $phone
     * @param array $vars
     * @param int $mmsTemplateId
     * @return mixed|null
     * author: wuchunhe
     * Date: 2021/6/16 3:18 下午
     */
    function sendMms($phone = [], $vars = [], $mmsTemplateId = 0)
    {
        $this->sms_msg->addPhoneList($phone);
        $this->sms_msg->setMsgType(MsgType::MMS);
        $this->sms_msg->addMapVars($vars);
        $this->sms_msg->setTemplateId($mmsTemplateId);
        $this->sms_msg->setTimestamp(time());
        $resonse = $this->send_sms->send($this->sms_msg);
        return $resonse->body();
    }
}